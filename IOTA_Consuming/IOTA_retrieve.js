# retrieve data streams from IOTA
const IOTA = require('iota.lib.js')
const iota = new IOTA({ provider: 'https://nodes.thetangle.org:443'})
const bundle ="GJHQJCRZRCPXJNNHSPSTEHTGKGINFMNCSHHQW9DJCBZFDGKXOAFKNHENYQBCYXJYNDDHLOZDKSNVJFNGD"
var searchVarsBundle = {'bundles':[bundle]}
var message
var bundles = new Set();

iota.api.findTransactions(searchVarsBundle, function(error, success) {
        if(error) {
                console.log(error)
                } else{
			iota.api.getBundle(success[0], function(error, success2){
				if(error){console.log(error)}else{
                                        message = success2[0].signatureMessageFragment;
					message = message.split("9").join("");
                           		console.log(iota.utils.fromTrytes(message));
					console.log("-----raw message print -----");
                                        console.log(success2[0].signatureMessageFragment);
                                }
                        })
        }
})
