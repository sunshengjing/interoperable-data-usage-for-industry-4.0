from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto import Random

# encryption
block_size = 16
def encryption_aes(meg):
    IV = Random.new().read(block_size)
    aes = AES.new('This is a key123', AES.MODE_CFB, IV)
    return aes.encrypt(meg)

def encryption_pub_pri(meg):
    modulus_length = 1024
    key = RSA.generate(modulus_length)
    publickey = key.publickey()
    encrypted = publickey.encrypt(meg.encode(), 32)
    return encrypted


