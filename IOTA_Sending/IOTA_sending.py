# Sending to IOTA

from iota import Address, ProposedTransaction, Tag, Transaction, Iota
from iota import TryteString
import iota
import time
import pandas as pd
import sys
import os
import ast
import json
import codecs
from IOTA_encryption import encryption_aes

SEED = 'BYX9IQGDXFDZZOVBVCZUOLGGTYYBMJRLGRLKUOPXAQ9BLSLOKXFSCQKEAODLTXFSIEMHATCAJHCTGJEW'
api = Iota('https://nodes.thetangle.org:443', SEED)

address=api.get_new_addresses(count=1)['addresses']

global stop
stop=False
def send_IOTA():
   
    json_file = open("./Load_Activity_Instance/load_activity_AOP0227.json", "r")
    data =json.load(json_file)
    print (json.dumps(data))
    print (str(encryption_aes(json.dumps(data))))
    api.send_transfer(
        transfers=[ProposedTransaction(
        address = Address(address[0]),
        message = TryteString.from_unicode(json.dumps(data)), 
        #message = TryteString.from_unicode(str(encryption_aes(json.dumps(data)))),
        tag     = Tag(b'LOADTRUCKTESTCASE'),
        value   = 0,
        )])

while stop==False:
    try:
        send_IOTA()
        print ('Message sent')
    except KeyboardInterrupt:
        stop=True
        sys.exit()
