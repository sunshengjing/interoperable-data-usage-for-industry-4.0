# Main goal 

Industry 4.0 mission fosters smart factories, which requires automation powered by data exchange in all manafacturing processes. The principles and goals of Industry 4.0 includes interconnection and information transparency, which are:

Interconnection: The ability of machines, devices, sensors, and people to connect and communicate with each other.
Information transparency: The transparency afforded by Industry 4.0 technology provides operators with vast amounts of useful information needed to make appropriate decisions.


To fulfill the mission, we have designed an architecture for implementing data interconnection and information transparency initiatives. The architecture enables to power knowledge based, data ownership supported, tamper-resist,  feeless and high-scalable data handling. Semantic modeling module is functional as heterogeneous data resource handling by applying an ontology-based information model. Data encription moudle applies encryption schemes to ensure data ownership.  IOTA as the next generation of distributed ledger technology, is designed specifically for IoT industry. Its tangle-based mechanism power secure, fee-less data integrity, high-scalable and tamper-resistant data sharing. 

![](img/architecture.png)


To evaluate the proposed architecture, we implemented a prototype. It encompasses two main parts:    


* [ ]  Part 1:  Data Injection (Sending) to IOTA Tangle. 
* [ ]  Part 2:  Data Consumption from IOTA Tangle.


# Getting Started

##  Part 1:  Data Sending to IOTA Tangle. 


### Data semantic modelling 

For data interoperability, all data collected from the industrial context will be semantically modeled. Here we take three data sources for example: 

Each worker is modeled based on existing ontology [Vcard People Ontology](https://www.w3.org/TR/vcard-rdf/). 
The working environmental situation is modeled on basis of [AIR_POLLUTION_Onto](http://dl.ifip.org/db/conf/ifip12/aiai2009/Oprea09.pdf).
The stress factor of each worker is modeled based on [HUMAN STRESS ONTOLOGY](https://espace.curtin.edu.au/handle/20.500.11937/42376).
To model product, product ontology and GoodRelations ontology can be reused  (https://www.w3.org/wiki/GoodRelations).

| entity | message |
| ------ | ------ |
| worker | { "Ontology":"https://bit.ly/2OxeEkO","object":"vcard","fn":"Perico de los Palotes Perez","nickname":"Peri","hasEmail":"mailto:perpalper@gmail.com","Gender":"Male","bday":"1988-06-23","adr":"José Gutiérrez Abascal 2, 28006, Madrid"} |
| environmental situation | {"Ontology": "AIR_POLLUTION_Onto", "object": "airPollutants", "deviceId": "Airmonitor1", "PM": "25ug/m3", "CO2": "0.04%", "VOC": "0.4mg/m3, "NOX": "0", "Timestramp": "09/08/2019 09:10:00"} | 
| stress factor | {"Ontology": "HUMAN STRESS ONTOLOGY", "object": "Measurements", "deviceId": "Hband1", Timestramp": "09/08/2019 09:10:00"}, "stressPhysiology": [{"heartReate": "80", "bloodPressureHigh": "120", "bloodPressureLow: "70"}]}|



### Sending data to IOTA Tangle

In order to interact with the IOTA network, you will need access to a node. The PyOTA documentation is functional as guide to implement data sending to Tangle.  [PyOTA Documentation](https://buildmedia.readthedocs.org/media/pdf/pyota/latest/pyota.pdf). 

*  TransactionHash: Identifies a transaction on the Tangle.
*  Seed: A TryteString that is used for crypto functions such as generating addresses, signing inputs, etc. 
*  TransactionTrytes: A TryteString representation of a transaction on the Tangle.
*  Bundle represents a bundle of transactions published on the Tangle


If you type the following command in your terminal, a example for sending data to IOTA Tangle would be launched.   

```  pip install pyota   ```


```  python IOTA_sending.py    ```


The message in the example is annotated with tag  [*LOADTRUCKTESTCASE*]. An individual bundle/address is assigned to each message on Tangle. Go to [The Tangle ](https://thetangle.org/), input the tag, you could check: 

![](img/transaction_activity.png)

##  Part 2:  Data Consuming from IOTA Tangle. 


### Retrieve data by bundle

The message publised on Tangle could be retrieved by bundle. The example message published on Tangle could be retrieved by run:


``` npm install iota.lib.js  ```

``` node IOTA_retrieve.js     ``` 


# Data security and Owership control  


Because on Tangle, anyone who has the tag, bundle or address of the message can download and read the contents.
To prevent unauthorized entities from reading the data, you could encrypt it before uploading it to the Tangle.  We have provided python script for encrypting the message, also for descrypting it.


* IOTA_ecryption.py 
* IOTA_decryption.py 

# About

These code are developed and maintained by Shengjing SUN (shengjing.sun@alumnos.upm.es).The following dev stacks are directly or indirectly supported at the moment:
   
*   Python
    
*   NodeJS 


